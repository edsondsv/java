/* 
 * Essa aplicação faz a conversão de temperatura de graus Celsius para graus Fahrenheit
 */

import java.util.Scanner;

public class Exerc03_simples {

	public static void main(String[] args) {
		// declaração de variáveis
		double fahrenheit, celsius;
				
		// leitura da temperatura
		Scanner teclado = new Scanner(System.in);
		System.out.print("Digite a temperatura em Celsius: ");
		celsius = teclado.nextDouble();
				
		// conversão da temperatura em graus Fahrenheit
		fahrenheit = (celsius / 5) * 9 + 32;
				
		// impressão na tela do valor já convertido
		System.out.println("Temperatura em graus Fahrenheit é: " + fahrenheit);

	}

}
