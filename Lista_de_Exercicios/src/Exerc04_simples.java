/* 
 * Essa aplicação faz o cálculo da quantidades de lâmpadas necessárias para iluminar uma área
 */

import java.util.Scanner;

public class Exerc04_simples {

	public static void main(String[] args) {
		// declaração de variáveis
		double pot_lamp, larg, comp, area, pot_total;
		int num_lamp;

		// leitura das informações
		Scanner teclado = new Scanner(System.in);
		
		System.out.print("Informe a potência da lâmpada em Watts: ");
		pot_lamp = teclado.nextDouble();
		
		System.out.print("Informe a largura do cômodo em metros: ");
		larg = teclado.nextDouble();
		
		System.out.print("Informe o comprimento do cômodo em metros: ");
		comp = teclado.nextDouble();
		
		// cálculos referentes a área e a quantidade de lâmpadas
		area = larg * comp;
		pot_total = 18 * area;
		num_lamp = (int) Math.round(pot_total / pot_lamp);
		
		// impressão dos resultados na tela
		System.out.println("O número de lâmpadas necessárias é: " + num_lamp);
	}
}
