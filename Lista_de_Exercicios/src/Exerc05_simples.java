/*
 * Calcular a quantidade de caixas de azulejos para colocar nas paredes de uma cozinha
 */

import java.util.Scanner;

public class Exerc05_simples {

	public static void main(String[] args) {
		// declaração de variáveis
		double comp, larg, alt, area;
		int caixas;
		
		// leitura das informações
		Scanner teclado = new Scanner(System.in);
		System.out.print("Informe o comprimento da cozinha: ");
		comp = teclado.nextDouble();
		
		System.out.print("Informe a lagura da cozinha: ");
		larg = teclado.nextDouble();
		
		System.out.print("Informe a altura da cozinha: ");
		alt = teclado.nextDouble();
		
		// cálculos realizados
		area = (comp * alt * 2) + (larg * alt * 2);
		caixas = (int) Math.round(area / 1.5);

		// mostrando os resultados na tela
		System.out.println("Quantidades de caixas de azulejos para colocar em todas as paredes: " + caixas);
	}
}
