/* 
 * Essa aplicação faz a leitura do raio de um círculo e calcula sua área
 */

import java.util.Scanner;

public class Exerc01_simples {

	public static void main(String[] args) {
		// declaração da constante PI
		final double PI = 3.14159;
		
		// declaração das variáveis locais, referentes ao círculo
		double raio, area;
		
		// Leitura do raio
		Scanner teclado = new Scanner(System.in);
		System.out.println("Digite o raio do círculo:");
		raio = teclado.nextDouble();
		
		// cálculo da área do círculo
		area = PI * pot_2(raio);
		
		// escrevendo na tela o resultado do cálculo
		System.out.println("Área do círculo é: " + area);
		
	}
	
	public static double pot_2 (double num) {
		return num * num;
	}

}