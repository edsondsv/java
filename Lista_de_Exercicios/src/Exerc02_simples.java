/* 
 * Essa aplicação faz a conversão de temperatura de graus Fahrenheit para graus Celsius
 */

import java.util.Scanner;

public class Exerc02_simples {

	public static void main(String[] args) {
		// declaração de variáveis
		double fahrenheit, celsius;
		
		// leitura da temperatura
		Scanner teclado = new Scanner(System.in);
		System.out.print("Digite a temperatura em Fahrenheit: ");
		fahrenheit = teclado.nextDouble();
		
		// conversão da temperatura em graus Celsius
		celsius = 5 * (fahrenheit - 32) / 9;
		
		// impressão na tela do valor já convertido
		System.out.println("Temperatura em graus Celsius é: " + celsius);

	}

}
