/**
 * @author 2017102227 - Edson dos Santos Vasconcelos
 */

import java.util.Scanner;

public class Funcoes {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int num;

        System.out.println("Digite um valor inteiro: ");
        num = teclado.nextInt();

        System.out.println("O valor do número é: " + num);
        System.out.println("Dobrando seu valor: " + dobra(num));
        
    }
    
    public static int dobra(int num) {
        return num * 2;
    }
}