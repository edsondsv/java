import java.util.Scanner;

public class Exerc_Arrays {
	
	static final int TAM = 5; // constante que define tamanho do vetor

	public static void main(String[] args) {
		
		int vetor[] = new int[TAM];
	
		// ler os valores do vetor
		lerVetor(vetor);
		
		// escrever os valores do vetor
		imprimeVetor(vetor);

	}
	
	// função que faz a leitura de valores do vetor
	public static void lerVetor(int[] vetor) {
		Scanner teclado = new Scanner(System.in);
		for (int i = 0; i < vetor.length; i++) {
			System.out.print("Entre o com valor " + (i+1) + ": ");
			vetor[i] = teclado.nextInt();
		}
	}
	
	// função qque faz a impressão dos valores do vetor
	public static void imprimeVetor(int[] vetor) {
		System.out.println("\n ----- VALORES DO VETOR: -----");
		for (int i = 0; i < vetor.length; i++) {
			System.out.println("valor " + (i+1) + ": " + vetor[i]);
		}
	}

}