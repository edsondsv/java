/**
 * @author 2017102227 - Edson dos Santos Vasconcelos
 */

import java.util.Scanner;

public class App02 {

    public static void main(String[] args) {
        int a,b = 0;
        double divisao = 0;
        Scanner teclado = new Scanner(System.in);
        
        System.out.println("Digite um valor inteiro:");
        a = teclado.nextInt();
        
        do {
            System.out.println("Digite outro valor inteiro:");
            b = teclado.nextInt();
        } while (b == 0);
        
        divisao = a / b;
        System.out.println("A divisão do primeiro valor pelo segundo é: " + divisao);
    }
    
}