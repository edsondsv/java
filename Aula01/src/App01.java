import java.util.Scanner;

/**
 * @author 2017102227 - Edson dos Santos Vasconcelos
 * @version 1.0
 */

public class App01 {

    public static void main(String[] args) {
        // declaração de varáveis
        double av1, av2, av3, media;
        Scanner teclado = new Scanner(System.in);
           
        // Entrada de Dados
        System.out.print("Digite a nota AV1: ");
        av1 = teclado.nextDouble();
        
        System.out.print("Digite a nota AV2: ");
        av2 = teclado.nextDouble();
        
        System.out.print("Digite a nota AV3: ");
        av3 = teclado.nextDouble();
        
        // Processamento
        media = (av1 + av2 + av3) /3;
        
        // Saída
        System.out.println("Média:" + media);
        
        if (media >=7) {
            System.out.println("APROVADO");
        } else {
            System.out.println("REPROVADO");
        }
    }
    
}